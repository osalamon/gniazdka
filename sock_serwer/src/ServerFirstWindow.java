import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import javax.swing.JTextField;

public class ServerFirstWindow extends JFrame{

	private static final long serialVersionUID = 1L;
	private JTextField userPassword;
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private JButton zatwierdzB;
	private JLabel podajH;
	private Socket connection;

	 
	
	public ServerFirstWindow(ObjectInputStream input2, ObjectOutputStream output2,Socket connection2) {

		setTitle("Hangman");
		setSize(250,200);
		setResizable(false);
		setLayout(null);
		input=input2;
		output=output2;
		connection=connection2;
		
		podajH = new JLabel("Podaj has�o");
		podajH.setBounds(50,20,150,40);
	
		
		userPassword=new JTextField("");
		userPassword.setBounds(50,50,150,40);
		userPassword.setEditable(true);

		
		
		zatwierdzB=new JButton("Zatwierdz haslo");
		zatwierdzB.setBounds(50,120,150,50);
		zatwierdzB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
			
					try {
						secondWindow();
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
			
		});
		
	
		add(podajH);
		setVisible(true);
		add(userPassword);
		add(zatwierdzB);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}
	
	public String readPassword(){
		String message;
		
		do {
			message = (String) userPassword.getText();
			System.out.println(message);
			sendMessage("HASLO:"+message);
			sendMessage("Zaczynamy");
		}while(message.equals(""));
		
		dispose();
		return message;
		
	}
	
	private void sendMessage(String message) {
		try {
			output.writeObject(message);
			output.flush();
			// showMessage("\nSERVER - "+ message);

		} catch (IOException ioException) {
			
		}
	}

	

	public void secondWindow() throws IOException, ClassNotFoundException{
		String haslo=readPassword();
		ServerSecondWindow ssw = new ServerSecondWindow(haslo,input,output,connection);
		ssw.setVisible(true);
	
	};
	

}
	
	
	
	
	

