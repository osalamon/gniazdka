import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class Server extends JFrame{

	
	private static final long serialVersionUID = 1L;
	private JTextArea chatWindow;
	public static ObjectOutputStream output;
	public static ObjectInputStream input;
	private Socket connection;

	

	private ServerSocket server;
	
	public Server() {
		
		setTitle("SERVER HANGMAN");
		setResizable(false);
		setSize(300,150);
		
		chatWindow= new JTextArea();
		add(new JScrollPane(chatWindow));
		chatWindow.setEditable(false);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}
	
	public void startRunning() {
		try {
			server = new ServerSocket(3145,10);
			while(true) {
				try {

					waitForConnection();
					setupStreams();
					firstWindow();
					dispose();
				
				
				}catch (EOFException eof) {
					showMessage("\n Serwer zakonczyl polaczenie!");
				}finally {
			
				}
			}
		}catch(IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	
	
	private void waitForConnection() throws IOException{
		showMessage("Czekam na polaczenie... \n");
		connection = server.accept();
		showMessage("Polaczaony z "+ connection.getInetAddress().getHostName());
	}
	
	
	
	
	
	private void setupStreams() throws IOException {
		output = new ObjectOutputStream(connection.getOutputStream());
		System.out.println(output);
		output.flush();
		input = new ObjectInputStream(connection.getInputStream());
		showMessage("\nPolaczenie jest gotowe\n");
		
	}
	
	

	
	private void showMessage(final String text) {
		SwingUtilities.invokeLater(
				new Runnable () {

					@Override
					public void run() {
						chatWindow.append(text);						
					}
				}
			);
		
	}
	
	


	public void firstWindow() {
		ServerFirstWindow sfw = new ServerFirstWindow(input,output,connection);
		sfw.setVisible(true);
		
		
	};
	
	
}
	
	
	
	
	

