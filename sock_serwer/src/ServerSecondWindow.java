import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;

public class ServerSecondWindow extends JFrame {

	private static final long serialVersionUID = 1L;
	private JTextArea chatWindow;
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private String message = "";
	private Socket connection;
	private JButton test;
	private JLabel label, pass, podane;
	private String haslo, hasloPokazane = "", wykorzystaneLiterki = "";
	public boolean gra = true;

	private ImageIcon image;
	private int counter = 1;

	private char[] tablicaHasla;

	public ServerSecondWindow(String haslo2, ObjectInputStream input2, ObjectOutputStream output2,Socket connection2)
			throws IOException, ClassNotFoundException {

		setTitle("HangmanServer");
		setSize(563, 563);
		setLayout(null);
		setResizable(false);

		input = input2;
		output = output2;
		connection=connection2;

		haslo = haslo2;
		System.out.println(haslo);
		image = new ImageIcon(getClass().getResource(counter + ".png"));
		label = new JLabel(image);
		label.setBounds(200, 150, 150, 150);
		add(label);
		haslo = haslo.toUpperCase();
		System.out.println(haslo);
		tablicaHasla = new char[haslo.length()];
		for (int i = 0; i < haslo.length(); i++) {
			tablicaHasla[i] = haslo.charAt(i);
			if (haslo.charAt(i) == ' ') {
				hasloPokazane += " ";
			} else
				hasloPokazane += "-";

		}

		test = new JButton("Start gry");
		test.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				new SwingWorker<Object, Object>() {

					@Override
					protected Object doInBackground() throws Exception {
						sendMessage("Jeste�my gotowi na gr�");
						readObjecty();
						return null;
					}

				}.execute();

			}

		});

		test.setBounds(350, 475, 200, 50);
		test.doClick();

		pass = new JLabel(hasloPokazane);
		pass.setBounds(200, 50, 550, 50);
		pass.setFont(new Font("Serif", Font.PLAIN, 24));
		add(pass);

		podane = new JLabel("");
		podane.setBounds(200, 375, 200, 100);
		pass.setFont(new Font("Serif", Font.PLAIN, 24));
		add(podane);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	private void close() {

		try {
			output.close();
			input.close();
			connection.close();
		} catch (IOException ioException) {
			ioException.printStackTrace();
		}
	}

	private void sendMessage(String message) {
		try {
			output.writeObject("SERVER - " + message);
			output.flush();

		} catch (IOException ioException) {
			chatWindow.append("\n ERROR: Nie moge wyslac tej wiadomosci");
		}
	}



	public void gameLoop(String litera) {
		char lit = Character.toUpperCase(litera.charAt(0));

		boolean zmianaZdjecia = true;

		for (int i = 0; i < haslo.length(); i++) {

			if (lit == haslo.charAt(i)) {

				hasloPokazane = hasloPokazane.substring(0, i) + lit + hasloPokazane.substring(++i);
				zmianaZdjecia = false;
			}
		}

		if (zmianaZdjecia == true) {
			image = new ImageIcon(getClass().getResource(++counter + ".png"));
			label.setIcon(image);
		}

		pass.setText(hasloPokazane);

		graSkonczona();

	}

	public void graSkonczona() {

		if (counter == 8) {
			sendMessage("Przegraless");
			JOptionPane.showMessageDialog(null, "Wygrales!", "Stan gry", JOptionPane.INFORMATION_MESSAGE);

			dispose();
			close();
		}
		if (hasloPokazane.equals(haslo)) {
			sendMessage("Wygrales. Pozostalo szans: " + (8 - counter));
			JOptionPane.showMessageDialog(null, "Przegrales!", "Stan gry", JOptionPane.INFORMATION_MESSAGE);

			dispose();
			close();
		}

	}

	public void readObjecty() throws ClassNotFoundException, IOException {
		do {
			try {
				message = (String) input.readObject();
				sendMessage(message);
				wykorzystaneLiterki = wykorzystaneLiterki + message.charAt(0) + ", ";
				gameLoop(message);

			} catch (ClassNotFoundException classN) {

			}
		} while (!message.equals("end"));

		System.out.println(message);
	}

}
