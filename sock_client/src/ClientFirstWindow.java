
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ClientFirstWindow extends JFrame {

	
	private static final long serialVersionUID = 1L;
	private JTextArea chatWindow;
	private JTextField userText,info;
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private Socket connection;
	private JLabel label, pass, podane;
	private String haslo, hasloPokazane = "", wykorzystaneLiterki = "";
	public boolean gra = true;
	private ImageIcon image;
	private int counter = 1,letterCounter=1;
	private char[] zbior;


	public ClientFirstWindow(String haslo2, ObjectInputStream input2, ObjectOutputStream output2,Socket connection2)
			throws IOException, ClassNotFoundException {

		setTitle("HangmanClient");
		setSize(563, 563);
		setLayout(null);
		setResizable(false);
		
		
		info=new JTextField("Podaj literke:");
		info.setEditable(false);
		info.setBounds(100,475,100,50);
		add(info);
		
		
		userText = new JTextField();
		userText.setEditable(true);
		userText.setBounds(200, 475, 50, 50);
		userText.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent event) {
						
						if(checkLetter(userText.getText().toUpperCase().charAt(0))) {
							gameLoop(userText.getText().toUpperCase());
							sendMessage(event.getActionCommand());
							graSkonczona();
							userText.setText("");
						}else {
							JOptionPane.showMessageDialog(null, "Powtorzyles literke: "+ userText.getText().charAt(0), "Powtorzenie", JOptionPane.INFORMATION_MESSAGE);
							userText.setText("");
						}
						
		
						
					}
					
				});
		add(userText);

		input = input2;
		output = output2;
		haslo = haslo2;
		connection=connection2;
		zbior=new char[24];
		System.out.println(haslo);
		
		image = new ImageIcon(getClass().getResource(counter + ".png"));
		label = new JLabel(image);
		label.setBounds(200, 150, 150, 150);
		add(label);

		haslo = haslo.toUpperCase();
		System.out.println(haslo);
		for (int i = 0; i < haslo.length(); i++) {
			if (haslo.charAt(i) == ' ') {
				hasloPokazane += " ";
			} else
				hasloPokazane += "-";

		}

		pass = new JLabel(hasloPokazane);
		pass.setBounds(200, 50, 500, 50);
		pass.setFont(new Font("Serif", Font.PLAIN, 24));
		add(pass);

		podane = new JLabel("");
		podane.setBounds(200, 375, 200, 100);
		pass.setFont(new Font("Serif", Font.PLAIN, 24));
		add(podane);

		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	
	public boolean checkLetter(char let) {
		boolean czy = false;
		for(int i=0;i<letterCounter;i++) {
			if(let==zbior[i]) {
				czy=false;
				i=letterCounter;
			}else if(i==letterCounter-1){
				zbior[i]=let;	
				System.out.println("PODAJE I "+i);
				letterCounter++;
				i++;
				czy=true;
			}
		}
		return czy;
	}
	


	private void close() {

		try {
			output.close();
			input.close();
			connection.close();
		} catch (IOException ioException) {
			ioException.printStackTrace();
		}
	}


	private void sendMessage(String message) {
		try {
			output.writeObject(message);
			output.flush();

		} catch (IOException ioException) {
			chatWindow.append("\n ERROR: Nie moge wyslac tej wiadomosci");
		}
	}


	public void gameLoop(String litera) {
		char lit = Character.toUpperCase(litera.charAt(0));
		boolean zmianaZdjecia = true;
		System.out.println(lit);
		int j=0;
		for (int i = 0; i < haslo.length(); i++) {

			if (lit == haslo.charAt(i)) {
				j=i+1;
				hasloPokazane = hasloPokazane.substring(0, i) + lit + hasloPokazane.substring(j);
				System.out.println(hasloPokazane);
				zmianaZdjecia = false;
			}
		}

		if (zmianaZdjecia == true) {
			image = new ImageIcon(getClass().getResource(++counter + ".png"));
			label.setIcon(image);
		}
		wykorzystaneLiterki=wykorzystaneLiterki+lit+", ";
		
		pass.setText(hasloPokazane);
		podane.setText(wykorzystaneLiterki);

	}

	public void graSkonczona() {

		if (counter == 8) {
			sendMessage("Przegrales");
			JOptionPane.showMessageDialog(null, "Przegrales!", "Stan gry", JOptionPane.INFORMATION_MESSAGE);
			dispose();
			close();
		}
		if (hasloPokazane.equals(haslo)) {
			sendMessage("Wygrales. Pozostalo szans: " + (8 - counter));
			JOptionPane.showMessageDialog(null, "Wygrales!", "Stan gry", JOptionPane.INFORMATION_MESSAGE);
			dispose();
			close();
		}
	}
}
