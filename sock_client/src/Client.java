import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Client extends JFrame{

	private static final long serialVersionUID = 1L;
	public JTextField userText;
	private JTextArea chatWindow;
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private String message = "";
	private String serverIP;
	private Socket connection;
	
	public Client(String host) {
		super("CLIENTHANGMAN");
		serverIP = host;
		userText = new JTextField();
		userText.setEditable(false);
		userText.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent event) {
						sendMessage(event.getActionCommand());
						userText.setText("");
					}
					
				});
		add(userText,BorderLayout.SOUTH);
		chatWindow=new JTextArea();
		chatWindow.setEditable(false);
		add(new JScrollPane(chatWindow), BorderLayout.CENTER);
		setSize(300,150);
		setVisible(true);
	}
	
	//connect to server
	
	public void startRunning() throws ClassNotFoundException {
		try {
			connectToServer();
			setupStreams();
			showMessage("Czekam na haslo\n");
			//whileChatting();
			firstWindow(getPassword());
			ableToType(true);
			dispose();
		}catch (EOFException eofEx) {
			showMessage("\n Zbyt dlugie oczekiwanie na polaczenie");
		}catch (IOException ioEx) {
			ioEx.printStackTrace();
		}finally {
			//closeCrap();
		}
	}
	
	public void firstWindow(String haslo) throws ClassNotFoundException, IOException {
	
		ClientFirstWindow cfw = new ClientFirstWindow(haslo,input,output,connection);
		cfw.setVisible(true);
			
		
	};
	
	//connect to server
	
	public String getPassword() throws ClassNotFoundException, IOException {
		String test="";
		do {
		test= (String) input.readObject();
		}while(test.substring(0,6)=="HASLO:");
		test=test.substring(6);
		System.out.println("PODZIELONE HASLO-"+test);
		showMessage("Mozemy zaczynac");
		return test;
	};
	
	private void connectToServer() throws IOException{
		showMessage("Proba polaczenia...\n");
		connection = new Socket(InetAddress.getByName(serverIP),3145);
		showMessage("Polaczono z: "+ connection.getInetAddress().getHostName());
		
	}
	
	//set up streams to send and receive messages
	
	private void setupStreams() throws IOException{
		output = new ObjectOutputStream(connection.getOutputStream());
		output.flush();
		input = new ObjectInputStream(connection.getInputStream());
		showMessage("\nPoczekaj na haslo od serwera.. \n");
	}
	
	//while chatting with server
	public void whileChatting() throws IOException{
		ableToType(true);
		do {
			try {
				message = (String) input.readObject();
				showMessage("\n"+message);
			}catch(ClassNotFoundException classNFX) {
				showMessage("\n Nie znam tego typu");
			}
			
			
		}while(!message.equals("SERVER - END"));
	}
	
	

	
	//send messages to server
	private void sendMessage(String message) {
		try {
			output.writeObject(message);
			output.flush();
			System.out.println(userText.getText());
			//showMessage("\nCLIENT - "+message);
			
		}catch(IOException ioEx) {
			chatWindow.append("\nCos poszlo nie tak!");
		}
	}
	
	//change/update chatWindow
	private void showMessage(final String m) {
		SwingUtilities.invokeLater(
				new Runnable() {

					@Override
					public void run() {
						chatWindow.append(m);
					}
					
				}
				);
		}
	
	// gives user permission to type into the text box
	private void ableToType(final boolean tof) {
		SwingUtilities.invokeLater(
				new Runnable() {

					@Override
					public void run() {
						userText.setEditable(tof);
					}
					
				}
			);
	}
	
}
